# Implementation of Small Projects using TensorFlow.js 

- check this [tutorial](https://medium.com/tensorflow/a-gentle-introduction-to-tensorflow-js-dba2e5257702) on TensorFlow.js
- [instruction page](https://wqw547243068.github.io/demo/page/)

## pix2pix 
Fast image-to-image check [demo](https://wqw547243068.github.io/demo/pix2pix/cats.html)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/pix2pix.png)

## fast-style 
Fast style transfer check [demo](https://wqw547243068.github.io/demo/fast-style/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/fast-style.PNG)

## Real Time Face Segmentation
Real Time Face Segmentation check [demo](https://wqw547243068.github.io/demo/face-segmentation)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/segmentation.png)

## Real Time style transfer
Real Time style transfer check [demo](https://wqw547243068.github.io/demo/RST/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/rst.png)

## Real Time Face recunstruction 
Face recunstuction [demo](https://wqw547243068.github.io/demo/fast-style/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/reconstruct.png)


## Texter 
Recognition of latex symbols check [demo](https://wqw547243068.github.io/demo/texter/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/texter.PNG)

## Sketcher 
Recognition of sketch drawings check [demo](https://wqw547243068.github.io/demo/sketcher/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/sketcher.PNG)

## Poser 
Track an object using your eyes  check [demo](https://wqw547243068.github.io/demo/poser/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/poser.PNG)

## Racer
Control a racing car using your eye movement check [demo](https://wqw547243068.github.io/demo/racer/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/racer.PNG)

## Sentiment Classification 
Given a movie review classify it as positive or negative check [demo](https://wqw547243068.github.io/demo/sentiment-classification/)

![alt text](https://raw.githubusercontent.com/zaidalyafeai/zaidalyafeai.github.io/master/images/sent-class.PNG)

